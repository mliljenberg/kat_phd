import React, { useContext, useState } from "react";

import { FormattedMessage } from "react-intl";
import TopBar from "../components/TopBar";
import styled from "styled-components";
import TextBlock from "../components/TextBlock";
import { Button, CircularProgress, Slider } from "@material-ui/core";
import Rating from "@material-ui/lab/Rating";
import short from "short-uuid";
import { storeContext } from "../store";

import silverImg from "../img/pen_silver.png";
import rubberImg from "../img/pen_rubber.png";
import manyImg from "../img/many_pens.png";
import fancyImg from "../img/nice_pen.png";
import Image from "../components/Image";
import Money from "../components/Money";

const translator = short("0123456789");
const fetchUrl =
  "https://gkxi0z6jxl.execute-api.us-east-1.amazonaws.com/default/Upload";

const pens = [
  { url: silverImg, id: "silver_pen", price: 4.23 },
  { url: rubberImg, id: "rubber_pen", price: 1.83 },
  { url: manyImg, id: "many_pen", price: 3.5 },
  { url: fancyImg, id: "fancy_pen", price: 3.1 }
];

const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-around;
`;

const LowWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
`;

const userId = translator.new().slice(0, 8);

// TODO: add questions that check what they thought about the information density?

export default function HighInfo() {
  const { dispatch, state } = useContext(storeContext);
  const [loading, setLoading] = useState(false);
  const [preDone, setPreDone] = useState(false);
  const [prePreDone, setPrePreDone] = useState(false);
  const [done, setDone] = useState(false);
  const [slider, setSlider] = useState(4);
  const decisions = [
    state.silver_pen,
    state.fancy_pen,
    state.many_pen,
    state.rubber_pen
  ];

  async function uploadResults(recommend) {
    setLoading(true);
    if (localStorage.getItem("done") === "true") {
      setPreDone(true);
    }
    const response = await fetch(fetchUrl, {
      method: "POST", // or 'PUT'
      mode: "no-cors",
      headers: {
        // "Content-Type": "application/json"
        "Content-Type": "text/plain"
      },
      body: JSON.stringify({
        ...state,
        friend_recommend: recommend,
        userId,
        first_time: !localStorage.getItem("done") === "true"
      }) // body data type must match "Content-Type" header
    });
    localStorage.setItem("done", "true");
    setDone(true);
  }

  if (prePreDone && !done && !preDone) {
    return (
      <div style={{ paddingTop: 40 }}>
        <div style={{ fontWeight: 600, fontSize: 20, margin: 50 }}>
          <FormattedMessage id="would_recommend" />
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            width: "100%"
          }}
        >
          <Slider
            style={{ width: 400 }}
            defaultValue={4}
            value={slider}
            onChange={(_, val) => setSlider(val)}
            aria-labelledby="discrete-slider"
            valueLabelDisplay="auto"
            step={1}
            marks
            min={1}
            max={7}
          />
          <Button
            style={{ margin: 20, marginBottom: 50, width: 120 }}
            variant="contained"
            color="primary"
            onClick={() => uploadResults(slider)}
          >
            <FormattedMessage id="buy" />
          </Button>
        </div>
      </div>
    );
  }

  if (preDone)
    return (
      <div style={{ fontWeight: 600, fontSize: 20, margin: 50 }}>
        <FormattedMessage id="already_done" />
      </div>
    );

  if (loading)
    return (
      <div style={{ padding: 40 }}>
        {done ? (
          <>
            <div>
              <FormattedMessage id="thank_you" />
            </div>
            <div>
              <FormattedMessage id="mturk" values={{ number: userId }} />
            </div>
          </>
        ) : (
          <>
            <div style={{ fontWeight: 600, fontSize: 20, margin: 20 }}>
              <FormattedMessage id="please_wait" />
            </div>
            <CircularProgress />
          </>
        )}
      </div>
    );

  return (
    <div>
      <meta name="viewport" content="width=1024"></meta>
      <TopBar />
      <div
        style={{
          fontSize: 25,
          fontWeight: 600,
          marginTop: 40
        }}
      >
        <FormattedMessage id="recommended_by" />{" "}
        <FormattedMessage
          id={state.person_id != null ? `${state.person_id}_name` : "ai"}
        />
      </div>
      <div style={{ fontWeight: 500, fontSize: 18, marginTop: 20 }}>
        <FormattedMessage id="what_to_do" />
      </div>
      {pens.map(({ url, id, price, text }) => (
        <div style={{ paddingTop: 30 }}>
          <Row
            key={id}
            url={url}
            price={price}
            id={id}
            info={state.info_density}
          />
        </div>
      ))}
      <div style={{ marginTop: 20, fontWeight: 600 }}>
        {decisions.filter(Boolean).length < 4 && (
          <FormattedMessage
            id="to_finish"
            values={{ decisions: decisions.filter(Boolean).length }}
          />
        )}
      </div>
      <Button
        style={{ margin: 20, marginBottom: 50 }}
        variant="contained"
        color="primary"
        disabled={decisions.filter(Boolean).length < 4}
        onClick={() => setPrePreDone(true)}
      >
        <FormattedMessage
          id="finish"
          values={{ decisions: decisions.filter(Boolean).length }}
        />
      </Button>
    </div>
  );
}

function Row(props) {
  const { url, text, price, id, info } = props;
  if (info === "high")
    return (
      <Wrapper>
        <Image url={url} />
        <TextBlock>
          <FormattedMessage id={id} />
        </TextBlock>
        <BuyBlock price={price} id={id} />
      </Wrapper>
    );
  return (
    <LowWrapper>
      <Image url={url} />
      <BuyBlock price={price} id={id} />
    </LowWrapper>
  );
}

const BlockWrapper = styled.div`
  background-color: white;
  padding: 20px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  width: 230px;
`;

function BuyBlock(props) {
  const { price, id } = props;
  const { dispatch, state } = useContext(storeContext);

  return (
    <BlockWrapper>
      {state[`${id}_done`] ? (
        <div style={{ fontWeight: 600, marginTop: "40%" }}>
          <FormattedMessage id="thanks_buy" />
        </div>
      ) : (
        <>
          <div>
            <FormattedMessage id="now_for_price" />:
          </div>

          <Money>
            <span style={{ fontSize: 22, fontWeight: 600 }}>
              <FormattedMessage id="currency" />
              {price}
            </span>
          </Money>

          <Slider
            defaultValue={4}
            value={state[id]}
            onChange={(_, val) => dispatch({ type: "pen", id, value: val })}
            aria-labelledby="discrete-slider"
            valueLabelDisplay="auto"
            step={1}
            marks
            min={1}
            max={7}
          />
          <div
            style={{ width: "100%", display: "flex", justifyContent: "center" }}
          >
            <Button
              style={{ width: 75 }}
              variant="contained"
              color="primary"
              onClick={() => dispatch({ type: "pen_done", id: id })}
            >
              <FormattedMessage id="buy" />
            </Button>
          </div>
        </>
      )}
    </BlockWrapper>
  );
}
