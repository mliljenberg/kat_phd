import React, { useState, useContext } from "react";
import {
  Card,
  CardActionArea,
  CardContent,
  Button,
  Avatar
} from "@material-ui/core";
import female1 from "../img/female1.jpg";
import female2 from "../img/female2.jpg";
import male1 from "../img/male1.jpg";
import male2 from "../img/male2.jpg";
import zhFemale1 from "../img/zh_female1.jpg";
import zhFemale2 from "../img/zh_female2.jpg";
import zhMale1 from "../img/zh_male1.jpg";
import zhMale2 from "../img/zh_male2.jpg";
import { storeContext } from "../store";
import { FormattedMessage, useIntl } from "react-intl";
import styled from "styled-components";
import ReactMarkdown from "react-markdown";
import { useHistory } from "react-router-dom";

const peopleList = [
  { id: "person1", img: male1, zhImg: zhMale1 },
  { id: "person2", img: female1, zhImg: zhFemale1 },
  { id: "person3", img: female2, zhImg: zhFemale2 },
  { id: "person4", img: male2, zhImg: zhMale2 }
];

export default function PersonSelect() {
  const [person, setPerson] = useState(null);
  const history = useHistory();
  const { dispatch, state } = useContext(storeContext);

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center"
      }}
    >
      {peopleList.map(({ id, img, zhImg }) => (
        <PersonBlock
          id={id}
          image={state.locale === "en" ? img : zhImg}
          selectedId={person}
          onClick={id => {
            setPerson(id);
            dispatch({ type: "person_id", value: id });
          }}
        />
      ))}
      <Button
        variant="contained"
        color="primary"
        disabled={person == null}
        onClick={() => {
          history.push(`/store?${state.query}`);
        }}
      >
        Next
      </Button>
    </div>
  );
}

function PersonBlock(props) {
  const { id, image, onClick, selectedId } = props;

  return (
    <Card
      style={{
        border: selectedId === id ? "black solid 2px" : "2px solid transparent",
        width: 400,
        height: 170,
        margin: 20
      }}
    >
      <CardActionArea
        style={{ width: "100%", height: "100%", padding: 20 }}
        onClick={() => onClick(id)}
      >
        <div style={{ display: "flex" }}>
          <div style={{ marginRight: 30 }}>
            <Avatar alt={id} src={image} />
            <div
              style={{
                marginTop: 10,
                fontWeight: 500,
                fontSize: 14,
                textAlign: "center"
              }}
            >
              <FormattedMessage id={`${id}_name`} />
            </div>
          </div>
          <div>
            <div style={{ textAlign: "left", fontSize: 14 }}>
              <FormattedMessage id={`${id}_text`} />
            </div>
          </div>
        </div>
      </CardActionArea>
    </Card>
  );
}
