import React, { useState, useContext } from "react";
import {
  Card,
  CardActionArea,
  CardContent,
  Button,
  CardMedia,
  Avatar
} from "@material-ui/core";
import { storeContext } from "../store";
import { FormattedMessage, useIntl } from "react-intl";
import styled from "styled-components";
import ReactMarkdown from "react-markdown";
import { useHistory } from "react-router-dom";

import item1 from "../img/item1.jpeg";
import item2 from "../img/item2.jpeg";
import item3 from "../img/item3.jpg";
import item6 from "../img/item4.jpg";
import item5 from "../img/item5.jpg";
import item4 from "../img/item6.jpg";

const itemList = [
  { id: "item1", img: item1 },
  { id: "item2", img: item2 },
  { id: "item3", img: item3 },
  { id: "item4", img: item4 },
  { id: "item5", img: item5 },
  { id: "item6", img: item6 }
];

const ItemRow = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  align-items: center;
`;

export default function PersonSelect() {
  const history = useHistory();
  const { dispatch, state } = useContext(storeContext);
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        flexWrap: "wrap",
        alignItems: "center"
      }}
    >
      <div style={{ fontSize: 18, fontWeight: 600 }}>
        <FormattedMessage id="select_3" />
      </div>
      <ItemRow>
        {itemList.slice(0, 3).map(({ id, img }) => (
          <ItemBlock
            id={id}
            image={img}
            selected={state.items.includes(id)}
            onClick={id => {
              dispatch({ type: "items", value: id });
            }}
          />
        ))}
      </ItemRow>
      <ItemRow>
        {itemList.slice(3).map(({ id, img }) => (
          <ItemBlock
            id={id}
            image={img}
            selected={state.items.includes(id)}
            onClick={id => {
              dispatch({ type: "items", value: id });
            }}
          />
        ))}
      </ItemRow>
      <div style={{ fontSize: 16, fontWeight: 600, margin: 20 }}>
        {state.items.length} <FormattedMessage id="selected" />
      </div>
      <Button
        variant="contained"
        color="primary"
        disabled={state.items.length < 3}
        onClick={() => {
          history.push(`/store?${state.query}`);
        }}
      >
        Next
      </Button>
    </div>
  );
}

function ItemBlock(props) {
  const { id, image, onClick, selected } = props;
  const intl = useIntl();

  return (
    <Card
      style={{
        border: selected ? "black solid 2px" : "2px solid transparent",
        width: 200,
        height: 200,
        margin: 20
      }}
    >
      <CardActionArea
        style={{ width: "100%", height: "100%" }}
        onClick={() => onClick(id)}
      >
        <CardMedia
          style={{ width: 180, height: 160, marginLeft: 10 }}
          image={image}
          title="image title"
        />
        {/* <div style={{ marginBottom: 10 }}>
          <ReactMarkdown source={intl.formatMessage({ id: id })} />
        </div> */}
      </CardActionArea>
    </Card>
  );
}
