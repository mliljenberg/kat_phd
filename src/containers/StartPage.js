import React, { useContext } from "react";
import { Select, MenuItem, InputLabel, Card, Button } from "@material-ui/core";
import { Link } from "react-router-dom";
import { storeContext } from "../store";
import { FormattedMessage } from "react-intl";
import styled from "styled-components";

const Wrapper = styled.div`
  width: 100vw;
  display: flex;
  min-height: 100vh;
  justify-content: center;
`;
const CardWrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding: 20px;
`;

export default function Start() {
  const { state, dispatch } = useContext(storeContext);
  return (
    <Wrapper>
      <Card style={{ width: 600, height: "70%" }}>
        <CardWrapper>
          <InputLabel style={{ margin: 20 }}>
            <FormattedMessage id="select_language" />
          </InputLabel>
          <Select
            style={{ width: 200 }}
            value={state.locale}
            onChange={event =>
              dispatch({ type: "change_locale", value: event.target.value })
            }
          >
            <MenuItem value="en">EN</MenuItem>
            <MenuItem value="zh">ZH</MenuItem>
          </Select>
          <Button
            component={Link}
            variant="contained"
            color="primary"
            to={`/condition?${state.query}`}
          >
            Start
          </Button>
        </CardWrapper>
      </Card>
    </Wrapper>
  );
}
