import React from "react";
import queryString from "query-string";

export const storeContext = React.createContext();

export let initialState = {
  locale: "zh",
  tracking: [],
  info_density: "high",
  condition: "person",
  person_id: null,
  fancy_pen: 4,
  silver_pen: 4,
  many_pen: 4,
  rubber_pen: 4,
  fancy_pen_done: false,
  silver_pen_done: false,
  many_pen_done: false,
  rubber_pen_done: false,
  items: [],
  query: "",
  done: false
};

initialState = getNewState(initialState);

export function reducer(state, action) {
  switch (action.type) {
    case "change_locale":
      return getNewState({ ...state, locale: action.value });
    case "person_id":
      return getNewState({ ...state, person_id: action.value });
    case "items":
      let newItems = [];
      if (state.items.includes(action.value)) {
        newItems = state.items.filter(val => action.value !== val);
        console.log("new", newItems);
      } else {
        newItems = [...state.items, action.value];
      }
      return getNewState({ ...state, items: newItems });
    case "pen":
      return getNewState({ ...state, [action.id]: action.value });
    case "pen_done":
      return getNewState({ ...state, [`${action.id}_done`]: true });
    case "track":
      return getNewState({
        ...state,
        tracking: [...state.track, action.value]
      });
    default:
      throw new Error();
  }
}

function getNewState(state) {
  const newState = state;
  delete newState.query;
  newState.query = queryString.stringify(newState);
  //   localStorage.setItem("store", JSON.stringify(state));
  return newState;
}
