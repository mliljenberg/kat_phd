import React from "react";
import styled from "styled-components";
// import ReactMarkdown from "react-markdown";

const Wrapper = styled.div`
  background-color: white;
  width: 60%;
  height: 270px;
  padding: 20px;
  text-align: left;
`;

export default function TextBlock(props) {
  const { children } = props;

  return <Wrapper>{children}</Wrapper>;
}
