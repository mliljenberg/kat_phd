import React from "react";

export default function Image(props) {
  let { url, width, height } = props;

  if (width == null) {
    width = 200;
  }

  if (height == null) {
    height = 200;
  }

  return (
    <img
      css={`
        width: ${width}px;
        height: ${height}px;
      `}
      src={url}
      alt="Something went wrong"
    />
  );
}
