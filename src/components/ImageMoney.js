import React from "react";
import Image from "./Image";
import Money from "./Money";

import styled from "styled-components";

const Wrapper = styled.div`
  background-color: white;
  width: 230px;
  height: 270px;
  display: flex;
  flex-direction: row;
`;

export default function ImageMoney(props) {
  const { url, text } = props;

  return (
    <Wrapper>
      <Image url={url} />
      <Money>{text}</Money>
    </Wrapper>
  );
}
