import styled from "styled-components";

export default styled.h5`
  font-weight: 600;
  color: black;
`;
