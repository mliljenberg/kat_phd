export const silverPenText = `The secret behind the unique capabilities of our pens lies in the ink cartridge itself. Our refills are hermetically sealed, pressurized with nitrogen gas, and feature a tungsten carbide ballpoint tip - the same extra hard metal that is used to make armor-piercing ammunition. No longer relying on gravity to write, our pens are able to function at any angle, even upside down! In order to prevent leaks from the gas pressure, our ink has to be thick, thixotropic to be exact. Fisher ink is actually more of a gel than a liquid when it is at rest, with a consistency similar to that of toothpaste. It is only when pressure is applied to the ballpoint that the ink will take on the spreadable properties of a liquid, while still retaining the stickiness needed to adhere to nearly any material.
\n\n
Facts and History of the Space Pens:
The story of the Space Pen really begins with its inventor, Paul C. Fisher. Before he took to making pens, Paul worked in an airplane propeller factory during World War II. It was there that he gained valuable experience with ball bearings, knowledge that would come in handy down the road for creating ballpoints`;

export const rubberPenText = `
PILOT DR. GRIP: This smooth-writing ergonomic ballpoint pen features our smear-resistant Advanced Ink & a wide barrel with an ergonomic, latex-free cushioned grip to help reduce writing stress.
\n\n
ERGONOMIC PEN: Commended for ease-of-use by the Arthritis Foundation, Pilot's Dr. Grip Ballpoint pen comes in medium & fine point & is available in blue, black, or red smear-resistant ink.
\n\n
PRESCRIPTION FOR WRITING COMFORT: If you love Pilot Dr. Grip ballpoint pens, try our full Dr. Grip line including Frosted, Multifunction, PureWhite, & FullBlack retractable, refillable ballpoint pens!
Retractable and refillable with G2 refills
\n\n
POWER TO THE PEN: Pilot makes exceptional writing instruments to suit all your needs. We have fountain, ballpoint, retractable, erasable & gel ink pens, whiteboard markers & more for every writing style.
`;

export const manyPenText = `
A bolder version of the classic BIC Cristal pen, the BIC Cristal Xtra Bold Ball Pen combines smooth ink flow with a sleek, refined design. This pen features Easy-Glide System ink, a proprietary technology that delivers vivid marks and is even smoother than the classic BIC ink system. The 1.6mm bold point creates strong, eye-catching lines, while the translucent smoked barrel allows you to easily monitor your remaining ink supply.
\n\n
The smoked barrel design of the Cristal Xtra Bold Ball Pen adds a sophisticated look to the original BIC pen. The translucent barrel still allows you to see how much ink you have left, while the tinted look distinguishes the Cristal Xtra Bold from other writing instruments. The classic hexagon-shaped barrel prevents the pen from rolling off flat surfaces.
`;

export const fancyPenText = `
The technology of Inkjoy: a unique blend of ultra-low viscosity inks and optimized writing tips gives you crisp, clean lines and unparalleled smoothness.
\n\n
The art of Inkjoy: a range of sleek and modern pen designs houses a rich palette of vivid ink colors to dazzle your writing imagination.
\n\n
InkJoy 700 pens have an hourglass shape sculpted to fit the contours of your hand and an integrated rubberized grip contour grip that is exceptionally comfortable to hold and easy to control.
\n\n
Inspired by the rich colors and high-gloss finish of a grand piano, this pen is the perfect blend of understated elegance and modern style. Solid metal accents provide durability.
\n\n
Tip retracts for convenient, one-hand use. 1.0 mm point gives you a nice, solid, smooth line for general writing. Accepts refills.
`;

export const fancyPenTextZh = `我们笔的独特功能背后秘密存在于墨盒本身之中。我们的笔芯是密封的，并用氮气加压，并具有碳化钨圆珠笔尖，所用材料与用于制造穿甲弹药的硬金属相同。不再依赖重力书写，我们的笔能够从任何角度书写，甚至可以倒过来书写！为了防止气压泄漏，我们的墨水必须浓稠，确切地说，我们的墨水是触变性的。静止时，费舍尔墨水实际更像是一种凝胶而不是液体，其稠度类似于牙膏。只有在向圆珠笔施加压力时，墨水才具有液体的可铺展特性，同时仍保持粘附到几乎任何材料上所需的粘性。
\ n \ n
太空笔的事实和历史：
太空笔的故事起源于其发明者保罗﹒费舍尔（Paul C. Fisher）。在造笔之前，保罗在第二次世界大战期间曾在飞机螺旋桨工厂工作。在那里，他获得了有关滚珠轴承的宝贵经验，这些知识在创建圆珠笔的过程中起到了很大的作用。
`;

export const manyPenTextZh = `百乐Dr. Grip品牌圆珠笔：这款书写流畅的人体工学圆珠笔，灌注的是防污高级墨水，具有符合人体工程学原理的宽笔筒和无乳胶缓冲握柄，有助于减轻书写时所需要的力度。
\ n \ n
符合人体工程学原理：百乐的Dr. Grip品牌圆珠笔，被关节炎基金会称赞为易用之笔，有中度和细度笔芯，所用防污墨水有蓝色的、黑色的、和红色的。
\ n \ n
舒适书写的方案：如果您喜欢百乐Dr. Grip品牌圆珠笔，就请使用我们Dr. Grip品牌的完整系列产品，包括Frosted、Multifunction、PureWhite和FullBlack！可伸缩、可填充的圆珠笔
可伸缩，可通过G2补充装
\ n \ n
笔的力量：百乐能制造出各种出色的书写工具，以满足您的所有需求。 我们有钢笔、圆珠笔，可伸缩、可擦掉的凝胶墨水笔、白板笔等等，适合各种书写风格。
`;

export const rubberPenTextZh = `BIC Cristal Xtra粗体原子笔是经典BIC Cristal笔的更改版本，结合了流畅的墨水流动和时尚精致的设计。这款产品采用易滑移系统墨水，这是一项新的技术，可提供生动的标记，并且比经典的BIC墨水系统还要光滑。 1.6mm的粗体笔芯可使线条清晰醒目，而半透明的烟熏桶使您可以轻松地掌控剩余的墨水。
\ n \ n
Cristal Xtra加粗圆珠笔的烟熏桶设计为原始BIC笔增添了精致的外观。 半透明的笔筒仍然可以让您看到还剩多少墨水，而有色外观使得Cristal Xtra Bold易于同其他书写工具区分开。 经典的六角形笔管可防止笔从平面滑落。
`;

export const silverPenTextZh = `Inkjoy的技术：超低粘度墨水与优化的书写笔尖的独特结合，可为您提供清晰，干净的线条和无与伦比的平滑度。
\ n \ n
Inkjoy的艺术：一系列时尚新颖的彩笔设计中，包含丰富生动的墨水调色板，可充分激发您的书写想象力。
\ n \ n
InkJoy 700笔具有沙漏形状，可以根据您的手部轮廓加以调整，而集成的橡胶握笔轮廓可使您握笔非常舒适，易于控制。
\ n \ n
灵感源自三角钢琴的丰富色彩和高光泽度，这款笔是低调优雅与现代风格的完美融合。固体金属装饰物具有耐用性。
\ n \ n
吸头缩回，方便单手使用。 1.0毫米的笔芯为您的书写提供了一条漂亮、坚实、流畅的线条。 可以补充墨水。
`;
