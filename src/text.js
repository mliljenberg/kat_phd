import {
  silverPenText,
  rubberPenText,
  manyPenText,
  fancyPenText,
  silverPenTextZh,
  rubberPenTextZh,
  manyPenTextZh,
  fancyPenTextZh
} from "./constants";

export const en_messages = {
  buy: "Buy now",
  currency: "$",
  select_language: "Select Language",
  person1_name: "Marcus",
  person2_name: "Kat",
  person3_name: "Anna",
  person4_name: "Peter",
  person1_text:
    "I am currently a student at a university. Most days, I spend my time studying or doing something with my friends after class. I commute to and from campus when I have to go to my classes. During lunch, I often eat with my friends. On the weekends, I try to unwind with a hobby, do something social, or catch up on some work. ",
  person2_text:
    "I am a university student. I attend classes on weekdays and usually do something with my friends on the weekends. Some weekends, I do something more relaxing or I try to catch up on some work. For my courses, I often have to read textbooks and try to learn the material. My time is often spent studying, meeting friends, or attending courses.",
  person3_text:
    "I am  currently a working professional. On weekdays, I usually commute to the office where I work. Although I enjoy socializing and meeting with my friends and family, sometimes my schedule is a bit busy. However, I usually try to find time to do something interesting on the weekends.",
  person4_text:
    "I am currently a working professional. I often have to travel to and from the office on weekdays. I spend my days working in the office until the evening when I commute home. Although I can be busy sometimes, I try to enjoy my weekends with friends or family. Sometimes, I also like to just relax at home.",
  item1: "item 1 name",
  item2: "item 2 name",
  item3: "item 3 name",
  item4: "item 4 name",
  item5: "item 5 name",
  item6: "item 6 name",
  recommended_by: "These pens where recommended for you by",
  what_to_do:
    "With the slider select how likely you are to buy each of the pens? \n 7 is very likely and 1 is un-likely",
  ai: "our ai",
  please_wait: "Please wait while we are processing your response",
  silver_pen: silverPenText,
  rubber_pen: rubberPenText,
  many_pen: manyPenText,
  fancy_pen: fancyPenText,
  thank_you: "Thank you for your participation in this study!",
  selected: "Items Selected, minimum 3 required",
  now_for_price: "Now for the price of",
  would_recommend:
    "How likely are you to recommend any of the pens to a friend. 1 (unlikely), 7 (very likely) ",
  item_select_explain:
    "Our AI will recommend you pens based on the products you like.",
  thanks_buy: "Thank you for using our store.",
  mturk: "If you used mturk here is your number: {number}",
  already_done:
    "It seems like you have done this study before, thanks for your participation",
  select_3: "Select at least 3 items that you like.",
  to_finish:
    "Please make a decision on each of the pens to continue: {decisions} of 4 decisions made.",
  done: "Done"
};

export const zh_messages = {
  buy: "现在购买",
  currency: "¥",
  select_language: "选择语言",
  person1_name: "张伟",
  person2_name: "芷若",
  person3_name: "语嫣",
  person4_name: "王芳",
  person1_text:
    "我是一名男性，目前在一所大学就读。大多数时候，我把时间花在学习或课后与我的朋友做一些事情方面。当我必须上课时，我就会通勤往返学校。我经常与朋友们一起吃午饭。周末时，我会试着找个爱好放松一下自己，做些社交活动，或者做些其他工作。",
  person2_text:
    "我是一名女大学生。周日我要上课，周末我通常和朋友在一起做些事情，有时候我会做一些更放松的事情，或者试着补充完成一些未能完成的工作。为了完成学业我经常要读课本，并努力学习与课本相关的材料。我经常把时间花在学习、见朋友或上课方面。",
  person3_text:
    "我是一名女性，目前正在工作。在工作日，我通常通勤到我工作的办公室。虽然我喜欢社交并与我的朋友和家人见面，但有时我的时间表比较满。我通常在周末找时间做一些有趣的事情。",
  person4_text:
    "我是一名男性，目前正在工作。工作日我通常必须往返办公室。我白天在办公室工作，直到晚上下班回家。虽然我有时会很忙，但我尽量试着和朋友或家人一起享受周末。有时候，我也喜欢就在家里放松一下。",
  selected: "Needs Translation",
  item1: "Needs Translation",
  item2: "Needs Translation",
  item3: "Needs Translation",
  item4: "Needs Translation",
  item5: "Needs Translation",
  item6: "Needs Translation",
  recommended_by: "这些笔在哪里为您推荐",
  ai: "我们的爱",
  what_to_do:
    "使用滑块选择您购买每支笔的可能性如何？ \n 7很有可能，而1不太可能",
  please_wait: "请稍候，我们正在处理您的回复",
  silver_pen: silverPenTextZh,
  rubber_pen: rubberPenTextZh,
  many_pen: manyPenTextZh,
  fancy_pen: fancyPenTextZh,
  mturk: "如果您使用mturk，请输入以下号码：{number}",
  item_select_explain: "我们的AI会根据您喜欢的产品向您推荐钢笔。",
  thanks_buy: "感谢您使用我们的商店",
  to_finish: "请对每支笔做出决定，以继续：做出4项决定中的{decisions}。",
  done: "完成"
};
