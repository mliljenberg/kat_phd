import React, { useReducer } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { IntlProvider } from "react-intl";

import "./App.css";
import StartPage from "./containers/StartPage";
import PersonSelect from "./containers/PersonSelect";
import ProductSelect from "./containers/ProductSelect";
import HighInfo from "./containers/HighInfo";

import { storeContext, reducer, initialState } from "./store";
import { en_messages, zh_messages } from "./text";
const lan = { en: en_messages, zh: zh_messages };

// const localState = JSON.parse(localStorage.getItem("done"));
// console.log("Hej", localState);
// const initState = localState != null ? localState : initialState;
function App() {
  const [state, dispatch] = useReducer(reducer, initialState);
  //prevent user from going back

  return (
    <storeContext.Provider value={{ state, dispatch }}>
      <IntlProvider locale={state.locale} messages={lan[state.locale]}>
        <div className="App">
          <Router>
            <Switch>
              <Route path="/condition">
                {state.condition === "person" ? (
                  <PersonSelect />
                ) : (
                  <ProductSelect />
                )}
              </Route>
              <Route path="/store">
                <HighInfo />
              </Route>
              <Route path="/done">
                <div>Last upload part here.</div>
              </Route>
              <Route>
                <StartPage />
              </Route>
            </Switch>
          </Router>
        </div>
      </IntlProvider>
    </storeContext.Provider>
  );
}

export default App;
